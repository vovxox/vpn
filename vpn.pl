use Tie::File;
use strict;
use warnings;

#
# Install pptpd package
#
system("sudo apt-get -y install pptpd");

#
# UTF Settting
#
system("sudo locale-gen en_AU en_AU.UTF-8 hu_HU hu_HU.UTF-8");

#
#Setting DHCP Pool
#
my $filename = "/etc/pptpd.conf";
my @array;
tie @array, 'Tie::File', $filename or die "can't tie file \"$filename\": $!";

for my $line (@array) {
  if($line =~ m/^\#(localip\s*)(192\.168\.0\.1)/){
        my $localip = " "; #This can be changed based on which Subnet the VPN instance will be sitting on
        $line = "$1$localip";
        chomp $line;
  }
  if($line =~ m/^\#(remoteip\s*)(192\.168\.0\..*)/){
        my $ip_range = " "; #DHCP Pool. This has to be changed based on the local ip range and gateway.
        $line = "$1$ip_range";
        chomp $line;
  }
}
untie @array;

#
#Setting DNS
#
$filename = "/etc/ppp/pptpd-options";
tie @array, 'Tie::File', $filename or die "can't tie file \"$filename\": $!";
for my $line (@array) {
  if($line =~ m/(^\#\s?)(ms\-dns\s+)(10\.0\.0\.1)/){
        my $primary_dns  = "8.8.8.8";
        $line = "$2$primary_dns";
  }
  if($line =~ m/(^\#\s?)(ms\-dns\s+)(10\.0\.0\.2)/){
        my $secondary_dns  = "8.8.4.4";
        $line = "$2$secondary_dns";
  }
}
untie @array;

#
# Setting to enable packet forwarding for Ipv4
#
$filename = "/etc/sysctl.conf";
tie @array, 'Tie::File', $filename or die "can't tie file \"$filename\": $!";
for my $line (@array) {
  if($line =~ m/^(\#)(net\.ipv4\.ip\_forward.*)/){
     $line = $2;
  }
}
untie @array;


system("sudo iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE");
$filename = "/etc/rc.local";
my $insert = 'sudo iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE';
tie @array, 'Tie::File', $filename or die "can't tie file \"$filename\": $!";
for my $line (@array) {
  if($line !~ m/"[^"].*"/){
     if($line =~ m/exit.*/){
        $line = "$insert\nexit 0";
     }
  }
}
untie @array;
#
# Setting authentication using CHAP, chap-secret
#
$filename = '/etc/ppp/chap-secrets';
my $authentication = '<%= options.VPNCredentials.id %>           pptpd  <%= options.VPNCredentials.password %>                   *';
open(my $fd, ">>$filename");
print $fd "$authentication";



#
# Restart VPN Service
#
system("sudo /etc/init.d/pptpd restart");
system("echo 'System will restart in 5 seconds'");
sleep(5);
#
# Restart Server
#
system("sudo reboot");
